@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    Soundboards
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- users list start -->
                <section class="">
                    <div class="card">
                        {{-- <div class="card-body border-bottom">
                            <h4 class="card-title mb-0">Search & Filter</h4>
                            <div class="row">
                                <div class="col-lg my-1">
                                    <label class="form-label" for="select2-basic">Basic</label>
                                    <select class="select2 form-select">
                                        <option value="AK">Alaska</option>
                                        <option value="AK">Alaska</option>
                                    </select>
                                </div>
                                <div class="col-lg my-1">
                                    <label class="form-label" for="select2-basic">Basic</label>
                                    <select class="select2 form-select">
                                        <option value="AK">Alaska</option>
                                    </select>
                                </div>
                                <div class="col-lg my-1">
                                    <label class="form-label" for="select2-basic">Basic</label>
                                    <select class="select2 form-select">
                                        <option value="AK">Alaska</option>
                                    </select>
                                </div>
                                <div class="col-lg-auto my-1 d-flex">
                                    <button class="btn btn-primary mt-auto">
                                        <i data-feather="filter" class="ficon me-1"></i>
                                        Filter
                                    </button>
                                </div>
                            </div>
                        </div> --}}
                        <div class="table-responsive">
                            <table class="table table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th width="10%">Action</th>
                                        <th width="10%">No</th>
                                        <th>Soundboard Title</th>
                                        <th>Soundboard Language</th>
                                        <th>Created By</th>
                                        <th>Soundboard Tags</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- list and filter end -->
                </section>
                <!-- users list ends -->
            </div>
        </div>
    </div>

    <!-- add new card modal  -->
    <div class="modal fade" id="frmbox" tabindex="-1" aria-labelledby="frmbox-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title">Form Soundboard</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-3 pb-2">
                    <!-- form -->
                    <form id="frm" class="row gy-1 gx-2 ">
                        @csrf
                        <input type="hidden" name="soundboard_id" id="soundboard_id">
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Soundboard Title</label>
                            <div class="input-group input-group-merge">
                                <input id="soundboard_title" name="inp[soundboard_title]" class="form-control add-credit-card-mask"
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Soundboard Video</label>
                            <div class="input-group input-group-merge">
                                <input id="soundboard_video" name="inp[soundboard_video]" class="form-control add-credit-card-mask"
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Soundboard Language</label>
                            <select class="select2 form-select" id="soundboard_lang_id" name="inp[soundboard_lang_id]" >
                                @foreach ($languages as $item)
                                    <option value="{{ $item->lang_id }}">{{ $item->lang_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Soundboard Tags</label>
                            <select class="select2 form-select" multiple id="soundboard_tags">
                                @foreach ($genres as $item)
                                    <option value="{{ $item->genre_id }}">{{ $item->genre_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">Soundboard Content</label>
                            <div id="full-wrapper">
                                <div id="full-container">
                                    <div id="editor" style="min-height:200px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <a class="btn btn-primary me-1 mt-1" onclick="save()">Submit</a>
                            <button type="reset" class="btn btn-outline-secondary mt-1" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/ add new card modal  -->
@endsection

@section('js_section')
    <script>
        var dTable = $('#table'),
            select = $('.select2')

        var fullEditor = new Quill('#full-container #editor', {
            bounds: '#full-container #editor',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                        font: ["sans-serif", "serif", "monospace", "poppins",'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                        color: [
                                "#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        // List datatable
        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/content/soundboards/dt') }}",
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false,
                        className: 'text-center'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'soundboard_title',
                        name: 'soundboard_title'
                    },
                    {
                        data: 'lang_name',
                        name: 'lang_name'
                    },
                    {
                        data: 'user_fullname',
                        name: 'user_fullname'
                    },
                    {
                        data: 'tags',
                        name: 'tags'
                    },
                ],
                order: [
                    [1, 'desc']
                ],
                buttons: [{
                    text: 'Add New',
                    className: 'add-new btn btn-primary btn-sm',
                    action: function(e, dt, node, config) {
                        addnew()
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }],
                "bFilter": false,
            });

            $('.dataTables_filter input[type=search]').attr('placeholder', 'Pencarian').attr('class',
                'form-control form-control-sm');
            $('.dataTables_filter select[name=table_length]').attr('class', 'form-select form-select-sm');
        })

        function addnew() {
            $('#frmbox').modal('show');
            $('#frm')[0].reset();
            $('#soundboard_tags').val('').trigger('change');
            $('#frm input[name="inp[soundboard_id]"]').val('');
            fullEditor.root.innerHTML = ''
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);
                formData.append('inp[soundboard_content]', fullEditor.root.innerHTML)
                formData.append('inp[soundboard_tags]', $('#soundboard_tags').val())
                $.ajax({
                    url: '{{ url('api/content/soundboards') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function edit(id) {
            $.ajax({
                url: '{{ url('api/content/soundboards/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                        'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });
                    if(e.soundboard_content) fullEditor.root.innerHTML = e.soundboard_content;
                    $('#soundboard_tags').val(JSON.parse(`[${e.soundboard_tags}]`)).trigger('change');
                        // $('#full-container #editor').html(e.soundboard_content);
                    $("#frmbox").modal('show');
                }
            });
        }

        function del(id) {
            if (confirm('Apakah anda yakin akan menghapus data ini?')) {
                $.ajax({
                    url: '{{ url('api/content/soundboards/') }}' + '/' + id,
                    type: 'delete',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }
    </script>
@endsection
