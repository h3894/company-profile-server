@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    Events
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- users list start -->
                <section class="">
                    <div class="card">
                        {{-- <div class="card-body border-bottom">
                            <h4 class="card-title mb-0">Search & Filter</h4>
                            <div class="row">
                                <div class="col-lg my-1">
                                    <label class="form-label" for="select2-basic">Basic</label>
                                    <select class="select2 form-select">
                                        <option value="AK">Alaska</option>
                                        <option value="AK">Alaska</option>
                                    </select>
                                </div>
                                <div class="col-lg my-1">
                                    <label class="form-label" for="select2-basic">Basic</label>
                                    <select class="select2 form-select">
                                        <option value="AK">Alaska</option>
                                    </select>
                                </div>
                                <div class="col-lg my-1">
                                    <label class="form-label" for="select2-basic">Basic</label>
                                    <select class="select2 form-select">
                                        <option value="AK">Alaska</option>
                                    </select>
                                </div>
                                <div class="col-lg-auto my-1 d-flex">
                                    <button class="btn btn-primary mt-auto">
                                        <i data-feather="filter" class="ficon me-1"></i>
                                        Filter
                                    </button>
                                </div>
                            </div>
                        </div> --}}
                        <div class="table-responsive">
                            <table class="table table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th width="10%">Action</th>
                                        {{-- <th width="10%">No</th> --}}
                                        <th>Event</th>
                                        <th>Price/Quota</th>
                                        <th>Location</th>
                                        <th>Type</th>
                                        <th>Period</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- list and filter end -->
                </section>
                <!-- users list ends -->
            </div>
        </div>
    </div>

    <!-- add new card modal  -->
    <div class="modal fade" id="frmbox" tabindex="-1" aria-labelledby="frmbox-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title">Form Article</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-3 pb-2">
                    <!-- form -->
                    <form id="frm" class="row gy-1 gx-2 ">
                        @csrf
                        <input type="hidden" name="event_id" id="event_id">
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">Event Title</label>
                            <div class="input-group input-group-merge">
                                <input id="event_title" name="inp[event_title]" class="form-control add-credit-card-mask"
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Event Subtitle</label>
                            <div class="input-group input-group-merge">
                                <input id="event_subtitle" name="inp[event_subtitle]"
                                    class="form-control add-credit-card-mask" type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Event Thumbnail</label>
                            <div class="input-group input-group-merge">
                                <input id="event_thumbnail" name="inp[event_thumbnail]"
                                    class="form-control add-credit-card-mask" type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Event Price</label>
                            <div class="input-group input-group-merge">
                                <input id="event_price" name="inp[event_price]" class="form-control add-credit-card-mask"
                                    type="number" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Event Quota</label>
                            <div class="input-group input-group-merge">
                                <input id="event_quota" name="inp[event_quota]" class="form-control add-credit-card-mask"
                                    type="number" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Event Location</label>
                            <div class="input-group input-group-merge">
                                <input id="event_location" name="inp[event_location]"
                                    class="form-control add-credit-card-mask" type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Event Redirect</label>
                            <div class="input-group input-group-merge">
                                <input id="event_redirect" name="inp[event_redirect]"
                                    class="form-control add-credit-card-mask" type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Event Type</label>
                            <select class="select2 form-select" name="inp[event_type]" id="event_type">
                                @foreach ($event_type as $item)
                                    <option value="{{ $item }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">Event Period</label>
                            <div class="row align-items-center" style="gap:10px">
                                <div class="col">
                                    <input id="event_startdate" name="inp[event_startdate]"
                                        class="form-control add-credit-card-mask" type="date" />
                                </div>
                                <div class="col-auto">
                                    to
                                </div>
                                <div class="col">
                                    <input id="event_enddate" name="inp[event_enddate]"
                                        class="form-control add-credit-card-mask" type="date" />
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">Event Content</label>
                            <div id="full-wrapper">
                                <div id="full-container">
                                    <div id="editor" style="min-height:200px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <a class="btn btn-primary me-1 mt-1" onclick="save()">Submit</a>
                            <button type="reset" class="btn btn-outline-secondary mt-1" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/ add new card modal  -->
@endsection

@section('js_section')
    <script>
        var dTable = $('#table'),
            select = $('.select2')

        var fullEditor = new Quill('#full-container #editor', {
            bounds: '#full-container #editor',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                        font: ["sans-serif", "serif", "monospace", "poppins",'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                        color: [
                                "#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        // List datatable
        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/content/events/dt') }}",
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false,
                        className: 'text-center'
                    },
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'event_title',
                        name: 'event_title'
                    },
                    {
                        data: 'event_price',
                        name: 'event_price'
                    },
                    {
                        data: 'event_location',
                        name: 'event_location'
                    },
                    {
                        data: 'event_type',
                        name: 'event_type'
                    },
                    {
                        data: 'event_period',
                        name: 'event_period'
                    },
                ],
                order: [
                    [1, 'desc']
                ],
                buttons: [{
                    text: 'Add New',
                    className: 'add-new btn btn-primary btn-sm',
                    action: function(e, dt, node, config) {
                        addnew()
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }],
                "bFilter": false,
            });

            $('.dataTables_filter input[type=search]').attr('placeholder', 'Pencarian').attr('class',
                'form-control form-control-sm');
            $('.dataTables_filter select[name=table_length]').attr('class', 'form-select form-select-sm');
        })

        function addnew() {
            $('#frmbox').modal('show');
            $('#frm')[0].reset();
            $('#event_type').val('').trigger('change');
            $('#frm input[name="inp[event_id]"]').val('');
            fullEditor.root.innerHTML = ''
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);
                formData.append('inp[event_desc]', fullEditor.root.innerHTML)
                $.ajax({
                    url: '{{ url('api/content/events') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function edit(id) {
            $.ajax({
                url: '{{ url('api/content/events/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                        'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });
                    if (e.event_desc) fullEditor.root.innerHTML = e.event_desc;
                    $("#frmbox").modal('show');
                }
            });
        }

        function del(id) {
            if (confirm('Apakah anda yakin akan menghapus data ini?')) {
                $.ajax({
                    url: '{{ url('api/content/events/') }}' + '/' + id,
                    type: 'delete',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }
    </script>
@endsection
