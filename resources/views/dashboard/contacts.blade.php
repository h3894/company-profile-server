@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    Contacts
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- users list start -->
                <section class="">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th width="10%">Action</th>
                                        <th>Client</th>
                                        <th>Company</th>
                                        <th>Status</th>
                                        <th>Received</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- list and filter end -->
                </section>
                <!-- users list ends -->
            </div>
        </div>
    </div>

    <!-- add new card modal  -->
    <div class="modal fade" id="frmbox" tabindex="-1" aria-labelledby="frmbox-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title">Form Contacts</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-3">
                    <div class="row gy-1 gx-2">
                        <input type="hidden" id="ct_id" />
                        <div class="col-6">
                            <label class="form-label">Client Name</label>
                            <div class="input-group input-group-merge">
                                <input id="ct_fullname" class="form-control" disabled type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Client Company</label>
                            <div class="input-group input-group-merge">
                                <input id="ct_company" class="form-control" disabled type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Client Email</label>
                            <div class="input-group input-group-merge">
                                <input id="ct_email" class="form-control" disabled type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Client Phone</label>
                            <div class="input-group input-group-merge">
                                <input id="ct_phone" class="form-control" disabled type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Industry</label>
                            <div class="input-group input-group-merge">
                                <input id="ct_industry" class="form-control" disabled type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Solution</label>
                            <div class="input-group input-group-merge">
                                <input id="ct_solution" class="form-control" disabled type="text" />
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="form-label">Received</label>
                            <div class="input-group input-group-merge">
                                <input id="ct_received" class="form-control" disabled type="text" />
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="form-label">Message</label>
                            <div class="input-group input-group-merge">
                                <textarea id="ct_message" class="form-control" disabled type="text" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer align-items-center">
                    <button type="button" class="btn btn-secondary btn-sm me-auto" data-bs-dismiss="modal">Close</button>

                    <button type="button" id="btn_pending" class="btn btn-success btn-sm" onclick="changeStatus('Y')" >Change Status To Done</button>
                    <button type="button" id="btn_done" class="btn btn-danger btn-sm" onclick="changeStatus('N')" >Reset Status</button>
                </div>
            </div>
        </div>
    </div>
    <!--/ add new card modal  -->
@endsection

@section('js_section')
    <script>
        var dTable = $('#table'),
            select = $('.select2')
        // List datatable
        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/dashboard/contacts/dt') }}",
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false,
                        className: 'text-center'
                    },
                    {
                        data: 'client',
                        name: 'client'
                    },
                    {
                        data: 'company',
                        name: 'company'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'ct_received',
                        name: 'ct_received'
                    },
                ],
                order: [[4, 'desc']],
                buttons: [],
                "bFilter": false,
            });

            $('.dataTables_filter input[type=search]').attr('placeholder', 'Pencarian').attr('class',
                'form-control form-control-sm');
            $('.dataTables_filter select[name=table_length]').attr('class', 'form-select form-select-sm');
        })

        function changeStatus(status) {
            let id = $('#ct_id').val()
            $.ajax({
                url: "{{ url('api/dashboard/contacts/status') }}",
                type: 'post',
                data: ({
                    id: id,
                    status
                }),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    dTable.draw()
                    new Noty({
                        text: 'Sukses mengubah status',
                        type: 'info',
                        progressBar: true,
                        timeout: 1000
                    }).show();
                    $('#frmbox').modal('hide')
                }
            })
        }

        function edit(id) {
            $.ajax({
                url: '{{ url('api/dashboard/contacts/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if(key == 'ct_status'){
                            if(value == 'Y') {
                                $('#btn_pending').hide()
                                $('#btn_done').show()
                            } else {
                                $('#btn_pending').show()
                                $('#btn_done').hide()
                            }
                        }

                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                        'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });
                    $("#frmbox").modal('show');
                }
            });
        }
    </script>
@endsection
