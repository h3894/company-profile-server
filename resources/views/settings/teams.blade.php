@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    Our Teams
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- users list start -->
                <section class="">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th width="10%">Action</th>
                                        <th width="10%">No</th>
                                        <th>Profile Pict</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Banner Pict</th>
                                        {{-- <th>Desc</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- list and filter end -->
                </section>
                <!-- users list ends -->
            </div>
        </div>
    </div>

    <!-- add new card modal  -->
    <div class="modal fade" id="frmbox" tabindex="-1" aria-labelledby="frmbox-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title">Form Teams</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-3 pb-2">
                    <!-- form -->
                    <form id="frm" class="row gy-1 gx-2 ">
                        @csrf
                        <input type="hidden" name="team_id" id="team_id">
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Profile Picture</label>
                            <div class="input-group input-group-merge">
                                <input id="team_profile_pict" name="inp[team_profile_pict]" class="form-control add-credit-card-mask" required
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Banner Picture</label>
                            <div class="input-group input-group-merge">
                                <input id="team_banner_pict" name="inp[team_banner_pict]" class="form-control add-credit-card-mask" required
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Name</label>
                            <div class="input-group input-group-merge">
                                <input id="team_name" name="inp[team_name]" class="form-control add-credit-card-mask" required
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Email</label>
                            <div class="input-group input-group-merge">
                                <input id="team_email" name="inp[team_email]" class="form-control add-credit-card-mask" required
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Role</label>
                            <div class="input-group input-group-merge">
                                <input id="team_role" name="inp[team_role]" class="form-control add-credit-card-mask" required
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Team</label>
                            <select id="team_team" name="inp[team_team]" class="form-control select2">
                                <option value="">-- Select Team --</option>
                                @foreach($categories as $team)
                                    <option value="{{ $team->tc_id }}">{{ $team->tc_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">MVP Link (Ex: LinkedIn)</label>
                            <div class="input-group input-group-merge">
                                <input id="team_redirect_mvp" name="inp[team_redirect_mvp]" class="form-control add-credit-card-mask" required
                                    type="text" />
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">Short Description</label>
                                <div id="full-wrapper ">
                                    <div id="full-container">
                                        <div id="editor-desc" class="w-100" style="min-height:200px">
                                            {{-- {!! $text !!} --}}
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">Full Biograph</label>
                                <div id="full-wrapper ">
                                    <div id="full-container">
                                        <div id="editor-bio" class="w-100" style="min-height:200px">
                                            {{-- {!! $text !!} --}}
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-12 text-center">
                            <a class="btn btn-primary me-1 mt-1" onclick="save()">Submit</a>
                            <button type="reset" class="btn btn-outline-secondary mt-1" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/ add new card modal  -->
@endsection

@section('js_section')
    <script>
        var dTable = $('#table'),
            select = $('.select2')
        // List datatable

        var fullEditorDESC = new Quill('#full-container #editor-desc', {
            bounds: '#full-container #editor-desc',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins",'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: [
                                "#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        var fullEditorBIO = new Quill('#full-container #editor-bio', {
            bounds: '#full-container #editor-bio',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins",'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: [
                                "#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/settings/teams/dt') }}",
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false,
                        className: 'text-center'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'team_profile_pict',
                        name: 'team_profile_pict'
                    },

                    {
                        data: 'team_name',
                        name: 'team_name'
                    },
                    {
                        data: 'team_role',
                        name: 'team_role'
                    },
                    {
                        data: 'team_banner_pict',
                        name: 'team_banner_pict'
                    },
                    // {
                    //     data: 'team_desc',
                    //     name: 'team_desc'
                    // },
                ],
                order: [
                    [1, 'desc']
                ],
                buttons: [{
                    text: 'Add New',
                    className: 'add-new btn btn-primary btn-sm',
                    action: function(e, dt, node, config) {
                        addnew()
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }],
                "bFilter": false,
            });

            $('.dataTables_filter input[type=search]').attr('placeholder', 'Pencarian').attr('class',
                'form-control form-control-sm');
            $('.dataTables_filter select[name=table_length]').attr('class', 'form-select form-select-sm');
        })

        function addnew() {
            $('#frmbox').modal('show');
            $('#frm')[0].reset();
            $('#team_id').val('');
            $('#team_name').val('');
            $('#team_role').val('');
            $('#team_team').val('');
            $('#team_profile_pict').val('');
            $('#team_banner_pict').val('');
            $('#team_redirect_mvp').val('');
            fullEditorDESC.root.innerHTML = ''
            fullEditorBIO.root.innerHTML = ''
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);
                formData.append('inp[team_desc]', fullEditorDESC.root.innerHTML)
                formData.append('inp[team_full_bio]', fullEditorBIO.root.innerHTML)
                $.ajax({
                    url: '{{ url('api/settings/teams') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function edit(id, dupe) {
            $.ajax({
                url: '{{ url('api/settings/teams/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                        'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            if(key == 'team_id' && dupe) {}
                            else $('#' + key).val(value);
                        }
                    });
                    if(e.team_desc) fullEditorDESC.root.innerHTML = e.team_desc;
                    if(e.team_full_bio) fullEditorBIO.root.innerHTML = e.team_full_bio;

                    $("#frmbox").modal('show');
                }
            });
        }

        function del(id) {
            if (confirm('Apakah anda yakin akan menghapus data ini?')) {
                $.ajax({
                    url: '{{ url('api/settings/teams/') }}' + '/' + id,
                    type: 'delete',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }
    </script>
@endsection
