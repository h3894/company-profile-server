@extends('layouts.layout1')

@section('css_section')
    <style>
        .dd {
            max-width: unset;
        }
    </style>
@endsection

@section('page_title')
    Product Category
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- users list start -->

                <div class="row">
                    <div class="col-lg-12">
                        <section class="card">
                            <div class="card-body ">
                                <div class="d-flex justify-content-between align-items-center mb-1">
                                    <h3 class="fw-bolder mb-0">Product Category</h3>
                                    <button onclick="addnew()" class="btn btn-primary btn-sm" style="margin-bottom: .5em">
                                        Add New Category
                                    </button>
                                </div>
                                <hr />
                                <div class="mb-1 d-flex" style="gap:10px">
                                    <div class="flex-fill">
                                        <select class="select2 form-select" id="categories">

                                        </select>
                                    </div>
                                    <button class="btn btn-primary d-flex align-items-center" style="gap:10px"
                                        onclick="edit()">
                                        <i class="bx bx-edit"></i>
                                        Edit
                                    </button>
                                </div>
                                <hr />
                                <div class="dd">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="1">
                                            <div class="dd-handle">Item 1</div>
                                        </li>
                                        <li class="dd-item" data-id="2">
                                            <div class="dd-handle">Item 2</div>
                                        </li>
                                        <li class="dd-item" data-id="3">
                                            <div class="dd-handle">Item 3</div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="4">
                                                    <div class="dd-handle">Item 4</div>
                                                </li>
                                                <li class="dd-item" data-id="5" data-foo="bar">
                                                    <div class="dd-nodrag">Item 5</div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <!-- list and filter end -->
                        </section>
                    </div>
                </div>
                <!-- users list ends -->
            </div>
        </div>
    </div>

    <!-- add new card modal  -->
    <div class="modal fade" id="frmbox" tabindex="-1" aria-labelledby="frmbox-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title">Form Product Category</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-3 pb-2">
                    <!-- form -->
                    <form id="frm" class="row gy-1 gx-2 ">
                        @csrf
                        <input type="hidden" name="procat_id" id="procat_id">
                        <div class="col-4">
                            <label class="form-label" for="modalAddCardNumber">Product Category Name</label>
                            <div class="input-group input-group-merge">
                                <input id="procat_name" name="inp[procat_name]" class="form-control add-credit-card-mask"
                                    required type="text" />
                            </div>
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="modalAddCardNumber">Product Category Parent</label>
                            <select class="select2 form-select" id="procat_parent" name="inp[procat_parent]">

                            </select>
                            <label class="text-muted">* opsional</label>
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="modalAddCardNumber">Product Category Image</label>
                            <div class="input-group input-group-merge">
                                <input id="procat_image" name="inp[procat_image]" class="form-control add-credit-card-mask"
                                    type="text" />
                            </div>
                            <label class="text-muted">* Hanya untuk kategori level 3 dan 4</label>
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="modalAddCardNumber">Product Category Layout</label>
                            <select class="select2 form-select" id="procat_layout_type" name="inp[procat_layout_type]"
                                required>
                                @foreach ($layouts as $item)
                                    <option value="{{ $item }}">{{ ucwords($item) }}</option>
                                @endforeach
                            </select>
                            <label class="text-muted">* Hanya untuk kategori level 4</label>
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="modalAddCardNumber">Product Redirect</label>
                            <div class="input-group input-group-merge">
                                <input id="procat_redirect" name="inp[procat_redirect]"
                                    class="form-control add-credit-card-mask" type="text" />
                            </div>
                            <label class="text-muted">* Hanya untuk kategori level 3 dan 4</label>
                        </div>

                        <div class="col-4">
                            <label class="form-label" for="modalAddCardNumber">As Leaf (Level 4)</label>
                            <select class="select2 form-select" id="procat_is_leaf" name="inp[procat_is_leaf]"
                                required>
                                @foreach (['yes', 'no'] as $item)
                                    <option value="{{ $item }}">{{ ucwords($item) }}</option>
                                @endforeach
                            </select>
                            <label class="text-muted">* Hanya untuk kategori level 3.</label>
                        </div>

                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Product Description</label>
                            <div id="full-wrapper h-100">
                                <div id="full-container">
                                    <div id="editor" class="w-100 h-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <label class="form-label" for="modalAddCardNumber">Product Description (English)</label>
                            <div id="full-wrapper h-100">
                                <div id="full-container">
                                    <div id="editor-eng" class="w-100 h-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 layout4">
                            <label class="form-label" for="modalAddCardNumber">Product Content Image</label>
                            <div class="input-group input-group-merge">
                                <input id="procat_content_image" name="inp[procat_content_image]"
                                    class="form-control add-credit-card-mask" type="text" />
                            </div>
                            <label class="text-muted">* Hanya untuk kategori level 3 dan 4</label>
                        </div>

                        <div class="col-6 layout4">
                            <label class="form-label" for="modalAddCardNumber">Product Content Description</label>
                            <div id="full-wrapper h-100">
                                <div id="full-container-editor">
                                    <div id="editor-content" class="w-100 h-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 layout4">
                            <label class="form-label" for="modalAddCardNumber">Product Content Description (English)</label>
                            <div id="full-wrapper h-100">
                                <div id="full-container-editor">
                                    <div id="editor-content-en" class="w-100 h-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 layout7">
                            <label class="form-label" for="modalAddCardNumber">Additional Content (1)</label>
                            <div id="full-wrapper ">
                                <div id="full-container-editor">
                                    <div id="editor-additional-1" class="w-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 layout7">
                            <label class="form-label" for="modalAddCardNumber">Additional Content (1) (English)</label>
                            <div id="full-wrapper ">
                                <div id="full-container-editor">
                                    <div id="editor-additional-1-en" class="w-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 layout7">
                            <label class="form-label" for="modalAddCardNumber">Additional Content (2)</label>
                            <div id="full-wrapper ">
                                <div id="full-container-editor">
                                    <div id="editor-additional-2" class="w-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 layout7">
                            <label class="form-label" for="modalAddCardNumber">Additional Content (2) (English)</label>
                            <div id="full-wrapper ">
                                <div id="full-container-editor">
                                    <div id="editor-additional-2-en" class="w-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 layout7">
                            <label class="form-label" for="modalAddCardNumber">Additional Content (3)</label>
                            <div id="full-wrapper ">
                                <div id="full-container-editor">
                                    <div id="editor-additional-3" class="w-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 layout7">
                            <label class="form-label" for="modalAddCardNumber">Additional Content (3) (English)</label>
                            <div id="full-wrapper ">
                                <div id="full-container-editor">
                                    <div id="editor-additional-3-en" class="w-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 layout7">
                            <label class="form-label" for="modalAddCardNumber">Additional Content (4)</label>
                            <div id="full-wrapper ">
                                <div id="full-container-editor">
                                    <div id="editor-additional-4" class="w-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 layout7">
                            <label class="form-label" for="modalAddCardNumber">Additional Content (4) (English)</label>
                            <div id="full-wrapper ">
                                <div id="full-container-editor">
                                    <div id="editor-additional-4-en" class="w-100" style="min-height:100px">
                                        {{-- {!! $text !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 d-flex justify-content-between">
                            <button type="reset" class="btn btn-outline-secondary mt-1" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <div class="d-flex" style="gap:10px">
                                <a id="content-edit" class="btn btn-secondary mt-1" target="_blank">
                                    Edit Content
                                </a>
                                <div class="d-flex layout7" style="gap:10px">
                                    <a id="content-edit-1" class="btn btn-secondary mt-1" target="_blank">
                                        Edit Content Section 1
                                    </a>
                                    <a id="content-edit-2" class="btn btn-secondary mt-1" target="_blank">
                                        Edit Content Section 2
                                    </a>
                                </div>
                                <button type="button" class="btn btn-outline-danger mt-1" onclick="del()">
                                    Delete
                                </button>
                                <a class="btn btn-primary mt-1" onclick="save()">Save</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!--/ add new card modal  -->
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
        $('.dd').nestable({
            maxDepth: 4,
            callback: function(l, e) {
                reorder($('.dd').nestable('serialize'))
            }
        });

        // document ready
        $(function() {
            drawTable()
        })


        var fullEditor = new Quill('#full-container #editor', {
            bounds: '#full-container #editor',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorENG = new Quill('#full-container #editor-eng', {
            bounds: '#full-container #editor',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        var fullEditorContent = new Quill('#full-container-editor #editor-content', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorContentEN = new Quill('#full-container-editor #editor-content-en', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        var fullEditorAdd1 = new Quill('#full-container-editor #editor-additional-1', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorAdd2 = new Quill('#full-container-editor #editor-additional-2', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorAdd3 = new Quill('#full-container-editor #editor-additional-3', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorAdd4 = new Quill('#full-container-editor #editor-additional-4', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorAdd1EN = new Quill('#full-container-editor #editor-additional-1-en', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorAdd2EN = new Quill('#full-container-editor #editor-additional-2-en', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorAdd3EN = new Quill('#full-container-editor #editor-additional-3-en', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });
        var fullEditorAdd4EN = new Quill('#full-container-editor #editor-additional-4-en', {
            bounds: '#full-container-editor #editor-content',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                            font: ["sans-serif", "serif", "monospace", "poppins", 'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                            color: ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        function reorder(params) {
            $.ajax({
                url: "{{ url('api/settings/procat/reorder') }}",
                type: 'post',
                dataType: 'json',
                data: ({
                    params,
                }),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    new Noty({
                        type: 'success',
                        text: 'Berhasil mengubah urutan',
                        timeout: 2000
                    }).show();
                }
            });
        }

        function drawTable() {
            $('.dd').nestable('removeAll', function() {
                console.log('All items deleted');
            });
            $('#procat_parent').empty()
            $('#procat_parent').append('<option value="">-- Select Category --</option>')
            $('#categories').empty()
            $('#categories').append('<option value="">-- Select Category --</option>')
            $.ajax({
                url: "{{ url('/api/settings/procat') }}",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $.each(data, function(key, value) {
                        $('#procat_parent').append('<option value="' + value.procat_id + '">' + value
                            .procat_name + '</option>')
                        $('#categories').append('<option value="' + value.procat_id + '">' + value
                            .procat_name + '</option>')
                        let params = {
                            id: value.procat_id,
                            content: `
                                <div class="d-flex justify-content-between align-items-center">
                                    <span id="category-item">${value.procat_name}</span>
                                </div>
                            `,
                        }
                        if (value.procat_parent) {
                            params.parent_id = value.procat_parent;
                        }
                        $('.dd').nestable('add', params)
                    });
                }
            });
        }

        $('#procat_layout_type').change(function() {
            if ($('#procat_layout_type').val() == 'layout4') {
                $('.layout7').hide()
                $('.layout4').show()
            }else if ($('#procat_layout_type').val() == 'layout7') {
                $('.layout4').show()
                $('.layout7').show()
            } else {
                $('.layout4').hide()
                $('.layout7 ').hide()
            }
        })

        function addnew() {
            $('#frmbox').modal('show');
            $('#frm')[0].reset();
            $('#procat_id').val('');
            $('#content-edit').hide()
            fullEditor.root.innerHTML = '';
            fullEditorContent.root.innerHTML = '';
            $('.layout4').hide()
            $('.layout7').hide()
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);
                formData.append('inp[procat_desc]', fullEditor.root.innerHTML);
                formData.append('inp[procat_desc_en]', fullEditorENG.root.innerHTML);
                formData.append('inp[procat_content]', fullEditorContent.root.innerHTML);
                formData.append('inp[procat_content_en]', fullEditorContentEN.root.innerHTML);
                formData.append('inp[procat_additional_1]', fullEditorAdd1.root.innerHTML);
                formData.append('inp[procat_additional_2]', fullEditorAdd2.root.innerHTML);
                formData.append('inp[procat_additional_3]', fullEditorAdd3.root.innerHTML);
                formData.append('inp[procat_additional_4]', fullEditorAdd4.root.innerHTML);
                formData.append('inp[procat_additional_1_en]', fullEditorAdd1EN.root.innerHTML);
                formData.append('inp[procat_additional_2_en]', fullEditorAdd2EN.root.innerHTML);
                formData.append('inp[procat_additional_3_en]', fullEditorAdd3EN.root.innerHTML);
                formData.append('inp[procat_additional_4_en]', fullEditorAdd4EN.root.innerHTML);
                $.ajax({
                    url: '{{ url('api/settings/procat') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            drawTable()
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function edit() {
            $('#content-edit').hide()
            $('#content-edit-1').hide()
            $('#content-edit-2').hide()
            let id = $('#categories').val()
            if (!id) {
                new Noty({
                    text: 'Please select category',
                    type: 'warning',
                    progressBar: true,
                    timeout: 1000
                }).show();;
                return false;
            } else
                $.ajax({
                    url: '{{ url('api/settings/procat/') }}' + '/' + id,
                    type: 'get',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        $.each(e, function(key, value) {
                            if ($('#' + key).hasClass("select2")) {
                                $('#' + key).val(value).trigger('change');
                            } else if ($('input[type=radio]').hasClass(key)) {
                                if (value != "") {
                                    $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                        'checked', true);
                                    $.uniform.update();
                                }
                            } else if ($('input[type=checkbox]').hasClass(key)) {
                                if (value != null) {
                                    var temp = value.split('; ');
                                    for (var i = 0; i < temp.length; i++) {
                                        $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                            'checked', true);
                                    }
                                    $.uniform.update();
                                }
                            } else {
                                $('#' + key).val(value);
                            }
                        });
                        if(e.procat_layout_type && e.procat_layout_type  == 'layout7'){
                            $('#content-edit-1').show()
                            $('#content-edit-2').show()
                            $('#content-edit-1').attr('href', '{{ url('/settings/product/') }}' + '/' + e
                                .procat_layout_type + '/' + id + '/1')
                            $('#content-edit-2').attr('href', '{{ url('/settings/product/') }}' + '/' + e
                                .procat_layout_type + '/' + id + '/2')
                        }
                        else if (e.procat_layout_type) {
                            $('#content-edit').attr('href', '{{ url('/settings/product/') }}' + '/' + e
                                .procat_layout_type + '/' + id)
                            $('#content-edit').show()
                        }
                        if (e.procat_desc) {
                            fullEditor.root.innerHTML = e.procat_desc;
                        } else fullEditor.root.innerHTML = ''

                        if (e.procat_desc_en) {
                            fullEditorENG.root.innerHTML = e.procat_desc_en;
                        }else fullEditorENG.root.innerHTML = ''

                        if (e.procat_content) {
                            fullEditorContent.root.innerHTML = e.procat_content;
                        }else fullEditorContent.root.innerHTML = ''

                        if (e.procat_content_en) {
                            fullEditorContentEN.root.innerHTML = e.procat_content_en;
                        }else fullEditorContentEN.root.innerHTML = ''

                        if (e.procat_additional_1) {
                            fullEditorAdd1.root.innerHTML = e.procat_additional_1;
                        }else fullEditorAdd1.root.innerHTML = ''

                        if (e.procat_additional_2) {
                            fullEditorAdd2.root.innerHTML = e.procat_additional_2;
                        }else fullEditorAdd2.root.innerHTML = ''

                        if (e.procat_additional_3) {
                            fullEditorAdd3.root.innerHTML = e.procat_additional_3;
                        }else fullEditorAdd3.root.innerHTML = ''

                        if (e.procat_additional_4) {
                            fullEditorAdd4.root.innerHTML = e.procat_additional_4;
                        }else fullEditorAdd4.root.innerHTML = ''

                        if (e.procat_additional_1_en) {
                            fullEditorAdd1EN.root.innerHTML = e.procat_additional_1_en;
                        }else fullEditorAdd1EN.root.innerHTML = ''

                        if (e.procat_additional_2_en) {
                            fullEditorAdd2EN.root.innerHTML = e.procat_additional_2_en;
                        }else fullEditorAdd2EN.root.innerHTML = ''

                        if (e.procat_additional_3_en) {
                            fullEditorAdd3EN.root.innerHTML = e.procat_additional_3_en;
                        }else fullEditorAdd3EN.root.innerHTML = ''

                        if (e.procat_additional_4_en) {
                            fullEditorAdd4EN.root.innerHTML = e.procat_additional_4_en;
                        }else fullEditorAdd4EN.root.innerHTML = ''
                        $("#frmbox").modal('show');
                    }
                });
        }

        function del() {
            let id = $('#categories').val()
            if (confirm('Apakah anda yakin akan menghapus data ini?')) {
                $.ajax({
                    url: '{{ url('api/settings/procat/') }}' + '/' + id,
                    type: 'delete',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            drawTable()
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }
    </script>
@endsection
