<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BE\LoginControllerAPI;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    /* Login */
    Route::POST('login', 'BE\LoginControllerAPI@exe');
    Route::GET('logout', 'BE\LoginControllerAPI@logout');

    /* Role Management */
    Route::POST('role/dt', 'BE\RoleControllerAPI@dt');
    Route::GET('role/{id}', 'BE\RoleControllerAPI@getById');
    Route::POST('role', 'BE\RoleControllerAPI@save');
    Route::DELETE('role/{id}', 'BE\RoleControllerAPI@delete');

    /* Genres Management */
    Route::POST('masterdata/genres/dt', 'BE\GenresControllerAPI@dt');
    Route::GET  ('masterdata/genres/json', 'BE\GenresControllerAPI@json');
    Route::GET('masterdata/genres/{id}', 'BE\GenresControllerAPI@getById');
    Route::POST('masterdata/genres', 'BE\GenresControllerAPI@save');
    Route::DELETE('masterdata/genres/{id}', 'BE\GenresControllerAPI@delete');

    /* Articles Management */
    Route::POST('content/articles/dt', 'BE\ArticlesControllerAPI@dt');
    Route::GET('content/articles/json', 'BE\ArticlesControllerAPI@json');
    Route::GET('content/articles/highlight', 'BE\ArticlesControllerAPI@highlight');
    Route::GET('content/articles/{id}', 'BE\ArticlesControllerAPI@getById');
    Route::POST('content/articles', 'BE\ArticlesControllerAPI@save');
    Route::DELETE('content/articles/{id}', 'BE\ArticlesControllerAPI@delete');

    /* Soundboards Management */
    Route::POST('content/soundboards/dt', 'BE\SoundboardControllerAPI@dt');
    Route::GET('content/soundboards/json', 'BE\SoundboardControllerAPI@json');
    Route::GET('content/soundboards/highlight', 'BE\SoundboardControllerAPI@highlight');
    Route::GET('content/soundboards/{id}', 'BE\SoundboardControllerAPI@getById');
    Route::POST('content/soundboards', 'BE\SoundboardControllerAPI@save');
    Route::DELETE('content/soundboards/{id}', 'BE\SoundboardControllerAPI@delete');

    /* Events Management */
    Route::POST('content/events/dt', 'BE\EventControllerAPI@dt');
    Route::GET('content/events/json', 'BE\EventControllerAPI@json');
    Route::GET('content/events/{id}', 'BE\EventControllerAPI@getById');
    Route::POST('content/events', 'BE\EventControllerAPI@save');
    Route::DELETE('content/events/{id}', 'BE\EventControllerAPI@delete');

    /* Language Management */
    Route::POST('settings/languages/dt', 'BE\Settings\LanguageControllerAPI@dt');
    Route::GET('settings/languages/json', 'BE\Settings\LanguageControllerAPI@json');
    Route::GET('settings/languages/{id}', 'BE\Settings\LanguageControllerAPI@getById');
    Route::POST('settings/languages', 'BE\Settings\LanguageControllerAPI@save');

    /* Page Management */
    Route::POST('settings/pages/dt', 'BE\Settings\PageControllerAPI@dt');
    Route::GET('settings/pages/json', 'BE\Settings\PageControllerAPI@json');
    Route::GET('settings/pages/{id}', 'BE\Settings\PageControllerAPI@getById');
    Route::POST('settings/pages', 'BE\Settings\PageControllerAPI@save');

    /* Page Management */
    Route::POST('settings/page-content/dt', 'BE\Settings\PageContentControllerAPI@dt');
    Route::GET('settings/page-content/json', 'BE\Settings\PageContentControllerAPI@json');
    Route::GET('settings/page-content/{id}', 'BE\Settings\PageContentControllerAPI@getById');
    Route::POST('settings/page-content', 'BE\Settings\PageContentControllerAPI@save');
    Route::DELETE('settings/page-content/{id}', 'BE\Settings\PageContentControllerAPI@delete');

    /* Client Management */
    Route::POST('settings/clients/dt', 'BE\Settings\ClientControllerAPI@dt');
    Route::GET('settings/clients/json', 'BE\Settings\ClientControllerAPI@json');
    Route::GET('settings/clients/{id}', 'BE\Settings\ClientControllerAPI@getById');
    Route::POST('settings/clients', 'BE\Settings\ClientControllerAPI@save');
    Route::DELETE('settings/clients/{id}', 'BE\Settings\ClientControllerAPI@delete');

    /* Team Category */
    Route::POST('settings/team-category/dt', 'BE\Settings\TeamCategoryControllerAPI@dt');
    Route::GET('settings/team-category/json', 'BE\Settings\TeamCategoryControllerAPI@json');
    Route::GET('settings/team-category/{id}', 'BE\Settings\TeamCategoryControllerAPI@getById');
    Route::POST('settings/team-category', 'BE\Settings\TeamCategoryControllerAPI@save');

    /* Teams */
    Route::POST('settings/teams/dt', 'BE\Settings\TeamControllerAPI@dt');
    Route::GET('settings/teams/json', 'BE\Settings\TeamControllerAPI@json');
    Route::GET('settings/teams/{id}', 'BE\Settings\TeamControllerAPI@getById');
    Route::POST('settings/teams', 'BE\Settings\TeamControllerAPI@save');
    Route::DELETE('settings/teams/{id}', 'BE\Settings\TeamControllerAPI@delete');

    /* Social Management */
    Route::get('settings/socmed/json', 'BE\Settings\SocialMediaControllerAPI@json');
    Route::POST('settings/socmed/dt', 'BE\Settings\SocialMediaControllerAPI@dt');
    Route::GET('settings/socmed/json', 'BE\Settings\SocialMediaControllerAPI@json');
    Route::GET('settings/socmed/{id}', 'BE\Settings\SocialMediaControllerAPI@getById');
    Route::POST('settings/socmed', 'BE\Settings\SocialMediaControllerAPI@save');
    Route::DELETE('settings/socmed/{id}', 'BE\Settings\SocialMediaControllerAPI@delete');

    // Product Category
    Route::GET('settings/procat', 'BE\Product\CategoryControllerAPI@list');
    Route::GET('settings/procat/dt', 'BE\Product\CategoryControllerAPI@dt');
    Route::GET('settings/procat/json', 'BE\Product\CategoryControllerAPI@json');
    Route::GET('settings/procat/{id}', 'BE\Product\CategoryControllerAPI@getById');
    Route::POST('settings/procat', 'BE\Product\CategoryControllerAPI@save');
    Route::POST('settings/procat/reorder', 'BE\Product\CategoryControllerAPI@reorder');
    Route::DELETE('settings/procat/{id}', 'BE\Product\CategoryControllerAPI@delete');

    // LAYOUT 1
    Route::GET('settings/product/layout1/json', 'BE\Product\Layout1ControllerAPI@json');
    Route::POST('settings/product/layout1/dt/{id}', 'BE\Product\Layout1ControllerAPI@dt');
    Route::GET('settings/product/layout1/{id}', 'BE\Product\Layout1ControllerAPI@getById');
    Route::POST('settings/product/layout1', 'BE\Product\Layout1ControllerAPI@save');
    Route::DELETE('settings/product/layout1/{id}', 'BE\Product\Layout1ControllerAPI@delete');
    Route::POST('/settings/product/layout1/child/dt/{id}', 'BE\Product\Layout1ControllerAPI@child_dt');
    Route::GET('settings/product/layout1/child/{id}', 'BE\Product\Layout1ControllerAPI@child_getById');
    Route::POST('settings/product/layout1/child', 'BE\Product\Layout1ControllerAPI@child_save');
    Route::DELETE('settings/product/layout1/child/{id}', 'BE\Product\Layout1ControllerAPI@child_delete');

    // LAYOUT 2
    Route::GET('settings/product/layout2/json', 'BE\Product\Layout2ControllerAPI@json');
    Route::POST('settings/product/layout2/dt/{id}', 'BE\Product\Layout2ControllerAPI@dt');
    Route::GET('settings/product/layout2/{id}', 'BE\Product\Layout2ControllerAPI@getById');
    Route::POST('settings/product/layout2', 'BE\Product\Layout2ControllerAPI@save');
    Route::DELETE('settings/product/layout2/{id}', 'BE\Product\Layout2ControllerAPI@delete');

    // LAYOUT 3
    Route::GET('settings/product/layout3/json', 'BE\Product\Layout3ControllerAPI@json');
    Route::POST('settings/product/layout3/dt/{id}', 'BE\Product\Layout3ControllerAPI@dt');
    Route::GET('settings/product/layout3/{id}', 'BE\Product\Layout3ControllerAPI@getById');
    Route::POST('settings/product/layout3', 'BE\Product\Layout3ControllerAPI@save');
    Route::DELETE('settings/product/layout3/{id}', 'BE\Product\Layout3ControllerAPI@delete');

    // LAYOUT 4
    Route::GET('settings/product/layout4/json', 'BE\Product\Layout4ControllerAPI@json');
    Route::POST('settings/product/layout4/dt/{id}', 'BE\Product\Layout4ControllerAPI@dt');
    Route::GET('settings/product/layout4/{id}', 'BE\Product\Layout4ControllerAPI@getById');
    Route::POST('settings/product/layout4', 'BE\Product\Layout4ControllerAPI@save');
    Route::DELETE('settings/product/layout4/{id}', 'BE\Product\Layout4ControllerAPI@delete');

    // LAYOUT 5
    Route::GET('settings/product/layout5/json', 'BE\Product\Layout5ControllerAPI@json');
    Route::POST('settings/product/layout5/dt/{id}', 'BE\Product\Layout5ControllerAPI@dt');
    Route::GET('settings/product/layout5/{id}', 'BE\Product\Layout5ControllerAPI@getById');
    Route::POST('settings/product/layout5', 'BE\Product\Layout5ControllerAPI@save');
    Route::DELETE('settings/product/layout5/{id}', 'BE\Product\Layout5ControllerAPI@delete');
    Route::POST('/settings/product/layout5/child/dt/{id}', 'BE\Product\Layout5ControllerAPI@child_dt');
    Route::GET('settings/product/layout5/child/{id}', 'BE\Product\Layout5ControllerAPI@child_getById');
    Route::POST('settings/product/layout5/child', 'BE\Product\Layout5ControllerAPI@child_save');
    Route::DELETE('settings/product/layout5/child/{id}', 'BE\Product\Layout5ControllerAPI@child_delete');

    // LAYOUT 6
    Route::GET('settings/product/layout6/json', 'BE\Product\Layout6ControllerAPI@json');
    Route::POST('settings/product/layout6/dt/{id}', 'BE\Product\Layout6ControllerAPI@dt');
    Route::GET('settings/product/layout6/{id}', 'BE\Product\Layout6ControllerAPI@getById');
    Route::POST('settings/product/layout6', 'BE\Product\Layout6ControllerAPI@save');
    Route::DELETE('settings/product/layout6/{id}', 'BE\Product\Layout6ControllerAPI@delete');
    Route::POST('/settings/product/layout6/child/dt/{id}', 'BE\Product\Layout6ControllerAPI@child_dt');
    Route::GET('settings/product/layout6/child/{id}', 'BE\Product\Layout6ControllerAPI@child_getById');
    Route::POST('settings/product/layout6/child', 'BE\Product\Layout6ControllerAPI@child_save');
    Route::DELETE('settings/product/layout6/child/{id}', 'BE\Product\Layout6ControllerAPI@child_delete');

    // LAYOUT 7_1
    Route::GET('settings/product/layout7_1/json', 'BE\Product\Layout7_1ControllerAPI@json');
    Route::POST('settings/product/layout7_1/dt/{id}', 'BE\Product\Layout7_1ControllerAPI@dt');
    Route::GET('settings/product/layout7_1/{id}', 'BE\Product\Layout7_1ControllerAPI@getById');
    Route::POST('settings/product/layout7_1', 'BE\Product\Layout7_1ControllerAPI@save');
    Route::DELETE('settings/product/layout7_1/{id}', 'BE\Product\Layout7_1ControllerAPI@delete');

    // LAYOUT 7_2
    Route::GET('settings/product/layout7_2/json', 'BE\Product\Layout7_2ControllerAPI@json');
    Route::POST('settings/product/layout7_2/dt/{id}', 'BE\Product\Layout7_2ControllerAPI@dt');
    Route::GET('settings/product/layout7_2/{id}', 'BE\Product\Layout7_2ControllerAPI@getById');
    Route::POST('settings/product/layout7_2', 'BE\Product\Layout7_2ControllerAPI@save');
    Route::DELETE('settings/product/layout7_2/{id}', 'BE\Product\Layout7_2ControllerAPI@delete');

    /* Dashboard Contacts */
    Route::POST('dashboard/contacts/dt', 'BE\Dashboard\ContactsControllerAPI@dt');
    Route::POST('dashboard/contacts/status', 'BE\Dashboard\ContactsControllerAPI@changeStatus');
    Route::GET('dashboard/contacts/save', 'BE\Dashboard\ContactsControllerAPI@save');
    Route::GET('dashboard/contacts/{id}', 'BE\Dashboard\ContactsControllerAPI@getById');
