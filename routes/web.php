<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::GET('/', 'FE\LoginController@index');
    Route::GET('login', 'FE\LoginController@login');

    Route::get('/nde/inbox', 'FE\InboxController@index');
    Route::get('/nde/role', 'FE\RoleController@index')->middleware('role:admin');

    Route::get('/masterdata/genres', 'FE\GenresController@index');

    Route::get('/content/articles', 'FE\ArticlesController@index');
    Route::get('/content/soundboards', 'FE\SoundboardController@index');
    Route::get('/content/events', 'FE\EventController@index');

    Route::get('/settings/languages', 'FE\Settings\LanguageController@index');
    Route::get('/settings/pages', 'FE\Settings\PageController@index');
    Route::get('/settings/page-content', 'FE\Settings\PageContentController@index');
    Route::get('/settings/clients', 'FE\Settings\ClientController@index');
    Route::get('/settings/team-category', 'FE\Settings\TeamCategoryController@index');
    Route::get('/settings/teams', 'FE\Settings\TeamController@index');
    Route::get('/settings/socmed', 'FE\Settings\SocialMediaController@index');

    // LAYOUT 2
    Route::get('/settings/procat', 'FE\Product\CategoryController@index');
    Route::get('/settings/product/layout1/{id}', 'FE\Product\Layout1Controller@index');
    Route::get('/settings/product/layout1/child/{id}', 'FE\Product\Layout1Controller@child');
    Route::get('/settings/product/layout2/{id}', 'FE\Product\Layout2Controller@index');
    Route::get('/settings/product/layout3/{id}', 'FE\Product\Layout3Controller@index');
    Route::get('/settings/product/layout4/{id}', 'FE\Product\Layout4Controller@index');
    Route::get('/settings/product/layout5/{id}', 'FE\Product\Layout5Controller@index');
    Route::get('/settings/product/layout5/child/{id}', 'FE\Product\Layout5Controller@child');
    Route::get('/settings/product/layout6/{id}', 'FE\Product\Layout6Controller@index');
    Route::get('/settings/product/layout6/child/{id}', 'FE\Product\Layout6Controller@child');
    Route::get('/settings/product/layout7/{id}/1', 'FE\Product\Layout7_1Controller@index');
    Route::get('/settings/product/layout7/{id}/2', 'FE\Product\Layout7_2Controller@index');

    Route::get('/dashboard/contacts', 'FE\Dashboard\ContactsController@index');
});
