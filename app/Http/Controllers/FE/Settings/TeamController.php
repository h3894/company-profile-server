<?php

namespace App\Http\Controllers\FE\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TeamCategory;

class TeamController extends Controller
{
    public function index()
    {

        $categories = TeamCategory::all();
        return view('settings.teams', compact('categories'));
    }
}
