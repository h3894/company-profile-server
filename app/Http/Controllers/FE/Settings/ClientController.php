<?php

namespace App\Http\Controllers\FE\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        return view('settings.clients');
    }
}
