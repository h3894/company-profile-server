<?php

namespace App\Http\Controllers\FE\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;

class PageContentController extends Controller
{
    public function index()
    {
        $languages = \DB::table('languages')->get();
        $pages = \DB::table('pages')->get();
        return view('settings.page-content', compact('languages', 'pages'));
    }
}
