<?php

namespace App\Http\Controllers\FE\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TeamCategoryController extends Controller
{
    public function index()
    {
        return view('settings.team_category');
    }
}
