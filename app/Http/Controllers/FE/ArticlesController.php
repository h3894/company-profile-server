<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Genres;

class ArticlesController extends Controller
{
    public function index()
    {
        $genres = Genres::all();
        $languages = \DB::table('languages')->get();
        return view('contents.articles', compact('genres', 'languages'));
    }
}
