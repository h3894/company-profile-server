<?php

namespace App\Http\Controllers\FE\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Layout6Controller extends Controller
{
    public function index($id)
    {
        $languages = \DB::table('languages')->get();
        $categories = \DB::table('product_category')->where('procat_id', $id)->first();
        return view('settings.product_category.layout6', compact('languages', 'categories', 'id'));
    }

    public function child($id){
        $section = \DB::table('product_layout6')->where('pl6_id', $id)->first();
        $categories = \DB::table('product_category')->where('procat_id', $section->pl6_procat_id)->first();
        return view('settings.product_category.layout6child', compact('section', 'categories', 'id'));
    }
}
