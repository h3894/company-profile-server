<?php

namespace App\Http\Controllers\FE\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $layouts = [
            'layout1', 'layout2', 'layout3', 'layout4', 'layout5', 'layout6', 'layout7'
        ];
        return view('settings.product_category.category', compact('layouts'));
    }
}
