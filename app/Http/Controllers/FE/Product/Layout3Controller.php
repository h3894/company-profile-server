<?php

namespace App\Http\Controllers\FE\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Layout3Controller extends Controller
{
    public function index($id)
    {
        $languages = \DB::table('languages')->get();
        $categories = \DB::table('product_category')->where('procat_id', $id)->first();
        return view('settings.product_category.layout3', compact('languages', 'categories', 'id'));
    }

}
