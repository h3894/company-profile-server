<?php

namespace App\Http\Controllers\FE\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Layout5Controller extends Controller
{
    public function index($id)
    {
        $languages = \DB::table('languages')->get();
        $categories = \DB::table('product_category')->where('procat_id', $id)->first();
        return view('settings.product_category.layout5', compact('languages', 'categories', 'id'));
    }

    public function child($id){
        $section = \DB::table('product_layout5')->where('pl5_id', $id)->first();
        $categories = \DB::table('product_category')->where('procat_id', $section->pl5_procat_id)->first();
        return view('settings.product_category.layout5child', compact('section', 'categories', 'id'));
    }
}
