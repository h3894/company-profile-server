<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index()
    {
        $event_type = ['Virtual Event', 'Training', 'Outdoor', 'Workshop'];
        return view('contents.events', compact('event_type'));
    }
}
