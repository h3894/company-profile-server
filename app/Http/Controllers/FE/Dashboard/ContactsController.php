<?php

namespace App\Http\Controllers\FE\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function index()
    {
        return view('dashboard.contacts');
    }
}
