<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Soundboard;
use App\Models\Genres;
use App\Models\UserActivity;

class SoundboardControllerAPI extends Controller
{

    public function highlight(Request $request){
        $query = "SELECT a.*, u.*, aa.views, bb.likes, cc.comments FROM soundboards a join users u on u.user_id = a.soundboard_createdby
        left join (
            select activity_targetid, count(activity_id) views from articles_user_activity aua where activity_type = 'view' and activity_targettype = 'soundboard'
            group by activity_targetid, activity_targettype
        ) aa on aa.activity_targetid = a.soundboard_id
        left join (
            select activity_targetid, count(activity_id) likes from articles_user_activity aua where activity_type = 'like' and activity_targettype = 'soundboard'
            group by activity_targetid, activity_targettype
        ) bb on bb.activity_targetid = a.soundboard_id
        left join (
            select activity_targetid, count(activity_id) comments from articles_user_activity aua where activity_type = 'comment' and activity_targettype = 'soundboard'
            group by activity_targetid, activity_targettype
        ) cc on cc.activity_targetid = a.soundboard_id ORDER by views DESC limit 2";

        $data = \DB::select($query);
        return $data;
    }

    public function json(Request $request){
        $query = "SELECT a.*, u.*, aa.views, bb.likes, cc.comments FROM soundboards a join users u on u.user_id = a.soundboard_createdby
        join languages lg on lg.lang_id = a.soundboard_lang_id
        left join (
            select activity_targetid, count(activity_id) views from articles_user_activity aua where activity_type = 'view' and activity_targettype = 'soundboard'
            group by activity_targetid, activity_targettype
        ) aa on aa.activity_targetid = a.soundboard_id
        left join (
            select activity_targetid, count(activity_id) likes from articles_user_activity aua where activity_type = 'like' and activity_targettype = 'soundboard'
            group by activity_targetid, activity_targettype
        ) bb on bb.activity_targetid = a.soundboard_id
        left join (
            select activity_targetid, count(activity_id) comments from articles_user_activity aua where activity_type = 'comment' and activity_targettype = 'soundboard'
            group by activity_targetid, activity_targettype
        ) cc on cc.activity_targetid = a.soundboard_id ";

        $page = $request->query('page');
        $perpage = $request->query('perpage');
        $genres = $request->query('genres');
        $lang = $request->query('lang');
        $id = $request->query('id');
        $search = strtolower($request->query('search'));
        $where = '';

        if($id){
            $soundboard = UserActivity::firstOrCreate([
                'activity_type' => 'view',
                'activity_targetid' => $id,
                'activity_targettype' => 'soundboard',
                'activity_source' => 'public',
                'activity_userid' => $request->ip()
            ]);
            $soundboard->activity_value = 'true';
            $soundboard->activity_recordedat = \Carbon\Carbon::now('Asia/Jakarta');
            $soundboard->save();
        }

        if($genres){
            $genres = explode(',',$genres);
            $where .= 'where 1=0';
            foreach($genres as $genre)
            $where .= " or LOCATE('$genre', soundboard_tags) != 0";
        }

        if($lang){
            if(str_contains($where, 'where')) $query .= ' and';
            else $where .= ' where';
            $where .= " lang_code = '$lang'";
        }

        if($id){
            if(str_contains($where, 'where')) $query .= ' and';
            else $where .= ' where';
            $where .= " soundboard_id = $id";
        }

        if($search){
            if(str_contains($where, 'where')) $query .= ' and';
            else $where .= ' where';
            $where .= " LOWER(soundboard_title) like '%$search%'";
        }
        $query = $query . $where;

        $count = count(\DB::select($query));

        if($page){
            $query .= " limit $perpage offset ".($page-1)*$perpage;
        }

        $data = \DB::select($query);
        foreach($data as $key => $value) {
            $tags = explode(',', $value->soundboard_tags);
            $genres = Genres::whereIn('genre_id', $tags)->get();
            $data[$key]->tags = $genres;
        }


        return ['data'=>$data,'total'=>$count];
    }

    public function dt()
    {
        $data = Soundboard::join('users', 'users.user_id', '=', 'soundboards.soundboard_createdby')->join('languages', 'languages.lang_id', '=', 'soundboards.soundboard_lang_id')->get();
        // $genres = Genres::all();

        foreach($data as $key => $value) {
            $tags = explode(',', $value->soundboard_tags);
            $genres = Genres::whereIn('genre_id', $tags)->get();
            $data[$key]->tags = $genres;
        }

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('tags', function($db){
                $tags = $db->tags;
                $html = '';
                foreach($tags as $key => $value) {
                    $html .= '<span class="badge bg-primary">'.$value->genre_name.'</span> ';
                }
                return $html;
            })
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->soundboard_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                        <a href="javascript:del(\''.$db->soundboard_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'tags'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Soundboard::find($request->soundboard_id) ?? new Soundboard();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            $dbs->soundboard_createdby = \Session::get('userId');
            $dbs->soundboard_lastupdate = \Carbon\Carbon::now('Asia/Jakarta');

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return Soundboard::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Soundboard::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //-----------------------------------------------------------------------

}
