<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Event;
use App\Models\Genres;

class EventControllerAPI extends Controller
{

    public function json(Request $request){
        $page = $request->query('page');
        $perpage = $request->query('perpage');
        $id = $request->query('id');
        $where = '';
        if($id){
            $where = "WHERE event_id = $id";
        }
        if($page && $perpage){
            $offset = ($page - 1) * $perpage;
            $where .= " LIMIT $perpage OFFSET $offset";
        }
        $query = "SELECT * FROM events $where";
        $query = \DB::select($query);
        $data = count(\DB::select("SELECT * FROM events"));

        return array(
            'total' => $data,
            'data' => $query
        );
    }

    public function dt()
    {
        $data = Event::all();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('event_period', function($db){
                $start = date('d M Y', strtotime($db->event_startdate));
                $end = date('d M Y', strtotime($db->event_enddate));
                $html = '<div>';
                $html .= '<span class="text-secondary">Start Date : <b>'.$start.'</b></span>';
                $html .= '<br>';
                $html .= '<span class="text-secondary">End Date : <b>'.$end.'</b></span>';
                $html .= '</div>';
                return $html;
            })
            ->editColumn('event_price', function($db){
                $price = number_format($db->event_price, 0, ',', '.');
                $quota = $db->event_quota;
                $html = '<div>';
                $html .= '<span class="text-secondary">Price : <b>Rp. '.$price.'</b></span>';
                $html .= '<br>';
                $html .= '<span class="text-secondary">Quota : <b>'.$quota.' Participants</b></span>';
                $html .= '</div>';
                return $html;
            })
            ->editColumn('event_title', function($db){
                $title = $db->event_title;
                $subtitle = $db->event_subtitle;
                $html = '<div>';
                $html .= '<span class="text-secondary"><b>'.$title.'</b></span>';
                $html .= '<br>';
                $html .= '<span class="text-secondary">'.$subtitle.'</span>';
                $html .= '</div>';
                return $html;
            })
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->event_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                        <a href="javascript:del(\''.$db->event_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'event_period', 'event_title', 'event_price'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Event::find($request->event_id) ?? new Event();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if(!isset($inp['event_price'])) $dbs->event_price = null;

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return Event::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Event::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //-----------------------------------------------------------------------

}
