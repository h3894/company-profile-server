<?php

namespace App\Http\Controllers\BE\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Contacts;

class ContactsControllerAPI extends Controller
{

    public function dt()
    {
        $data = \DB::select('select * from mail_contacts mc order by ct_received desc');

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->ct_id.'\')" title="Detail Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bxs-search-alt-2"></i></a>';
            })
            ->addColumn('client', function($db){
                return '
                    <h5 class="fw-bolder mb-0">'.$db->ct_fullname.'</h5>
                    <caption class="text-muted">'.$db->ct_phone.'</caption>
                ';
            })
            ->addColumn('company', function($db){
                return '
                    <h5 class="fw-bolder mb-0">'.$db->ct_company.'</h5>
                    <caption class="text-muted">'.$db->ct_industry.'</caption>
                ';
            })
            ->addColumn('status', function($db){
                $status = '<span class="badge bg-success">Done</span>';
                if($db->ct_status == 'N'){
                    $status = '<span class="badge bg-warning">Pending</span>';
                }

                return $status;
            })
            ->rawColumns(['action', 'client', 'company', 'status'])->toJson();
    }

    public function changeStatus(Request $request){
        $id = $request->id;
        $status = $request->status;

        $data = Contacts::find($id);
        $data->ct_status = $status;
        $data->save();
        return 'nice';
    }

    public function save(Request $request)
    {
        $data =  new Contacts();
        $data->ct_fullname = $request->fullname;
        $data->ct_phone = $request->phone;
        $data->ct_email = $request->email;
        $data->ct_company = $request->company;
        $data->ct_industry = $request->industry;
        $data->ct_message = $request->message;
        $data->ct_solution = $request->solution;
        $data->ct_status = 'N';
        $data->ct_received = date('Y-m-d H:i:s');
        $data->save();

        return 'nice';
    }

    public function getById($id)
    {
        return Contacts::find($id)->toJson();
    }

}
