<?php

namespace App\Http\Controllers\BE\Product;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Layout\Layout5;
use App\Models\Layout\Layout5Child;

class Layout5ControllerAPI extends Controller
{

    public function json(Request $request){
        $id = $request->query('id');
        $langcode = $request->query('langcode');
        $lang = \DB::table('languages')->where('lang_code', $langcode)->first();

        $layout = Layout5::where('pl5_lang_id', $lang->lang_id)->where('pl5_procat_id', $id)->orderBy('pl5_order')->get();

        foreach($layout as $item){
            $childs = Layout5Child::where('pl5c_pl5_id', $item->pl5_id)->get();
            $item['childrens'] = $childs;
        }

        return $layout;
    }

    public function dt($id)
    {
        $data = Layout5::join('languages', 'languages.lang_id', '=', 'product_layout5.pl5_lang_id')
            ->join('product_category', 'product_category.procat_id', '=', 'product_layout5.pl5_procat_id')
            ->where('procat_id', $id)->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->pl5_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->pl5_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'pl5_header'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Layout5::find($request->pl5_id) ?? new Layout5();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return Layout5::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Layout5::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    // child
    public function child_dt($id)
    {
        $data = Layout5Child::join('product_layout5', 'product_layout5.pl5_id', '=', 'product_layout5_child.pl5c_pl5_id')
            ->join('languages', 'languages.lang_id', '=', 'product_layout5.pl5_lang_id')
            ->join('product_category', 'product_category.procat_id', '=', 'product_layout5.pl5_procat_id')
            ->where('pl5c_pl5_id', $id)->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->pl5c_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->pl5c_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'pl5c_content'])->toJson();
    }

    public function child_save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Layout5Child::find($request->pl5c_id) ?? new Layout5Child();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function child_getById($id)
    {
        return Layout5Child::find($id)->toJson();
    }

    public function child_delete($id)
    {
        try {
            Layout5Child::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }
}
