<?php

namespace App\Http\Controllers\BE\Product;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Layout\Layout1;
use App\Models\Layout\Layout1Child;

class Layout1ControllerAPI extends Controller
{

    public function json(Request $request){
        $id = $request->query('id');
        $langcode = $request->query('langcode');
        $lang = \DB::table('languages')->where('lang_code', $langcode)->first();

        $layout = Layout1::where('pl1_lang_id', $lang->lang_id)->where('pl1_procat_id', $id)->orderBy('pl1_order')->get();

        foreach($layout as $item){
            $childs = Layout1Child::where('pl1c_pl1_id', $item->pl1_id)->get();
            $item['childrens'] = $childs;
        }

        return $layout;
    }

    public function dt($id)
    {
        $data = Layout1::join('languages', 'languages.lang_id', '=', 'product_layout1.pl1_lang_id')
            ->join('product_category', 'product_category.procat_id', '=', 'product_layout1.pl1_procat_id')
            ->where('procat_id', $id)->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->pl1_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->pl1_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'pl1_header'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Layout1::find($request->pl1_id) ?? new Layout1();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return Layout1::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Layout1::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    // child
    public function child_dt($id)
    {
        $data = Layout1Child::join('product_layout1', 'product_layout1.pl1_id', '=', 'product_layout1_child.pl1c_pl1_id')
            ->join('languages', 'languages.lang_id', '=', 'product_layout1.pl1_lang_id')
            ->join('product_category', 'product_category.procat_id', '=', 'product_layout1.pl1_procat_id')
            ->where('pl1c_pl1_id', $id)->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->pl1c_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->pl1c_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'pl1c_content'])->toJson();
    }

    public function child_save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Layout1Child::find($request->pl1c_id) ?? new Layout1Child();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function child_getById($id)
    {
        return Layout1Child::find($id)->toJson();
    }

    public function child_delete($id)
    {
        try {
            Layout1Child::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }
}
