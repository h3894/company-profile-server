<?php

namespace App\Http\Controllers\BE\Product;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Layout\Layout4;
use App\Models\Layout\Layout4Child;

class Layout4ControllerAPI extends Controller
{

    public function json(Request $request){
        $id = $request->query('id');
        $langcode = $request->query('langcode');
        $lang = \DB::table('languages')->where('lang_code', $langcode)->first();

        $layout = Layout4::where('pl4_lang_id', $lang->lang_id)->where('pl4_procat_id', $id)->orderBy('pl4_order')->get();

        return $layout;
    }

    public function dt($id)
    {
        $data = Layout4::join('languages', 'languages.lang_id', '=', 'product_layout4.pl4_lang_id')
            ->join('product_category', 'product_category.procat_id', '=', 'product_layout4.pl4_procat_id')
            ->where('procat_id', $id)->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->pl4_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->pl4_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->editColumn('pl4_image', function($db){
                if($db->pl4_image){
                    return "<a target='_blank' href='".$db->pl4_image."'>Image Link</a>";
                } else return 'no image';
            })
            ->rawColumns(['action', 'pl4_header', 'pl4_content', 'pl4_image'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Layout4::find($request->pl4_id) ?? new Layout4();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return Layout4::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Layout4::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }


}
