<?php

namespace App\Http\Controllers\BE\Product;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Layout\Layout3;
use App\Models\Layout\Layout3Child;

class Layout3ControllerAPI extends Controller
{

    public function json(Request $request){
        $id = $request->query('id');
        $langcode = $request->query('langcode');
        $lang = \DB::table('languages')->where('lang_code', $langcode)->first();

        $layout = Layout3::where('pl3_lang_id', $lang->lang_id)->where('pl3_procat_id', $id)->orderBy('pl3_order')->get();

        return $layout;
    }

    public function dt($id)
    {
        $data = Layout3::join('languages', 'languages.lang_id', '=', 'product_layout3.pl3_lang_id')
            ->join('product_category', 'product_category.procat_id', '=', 'product_layout3.pl3_procat_id')
            ->where('procat_id', $id)->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->pl3_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->pl3_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'pl3_content'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Layout3::find($request->pl3_id) ?? new Layout3();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return Layout3::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Layout3::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }


}
