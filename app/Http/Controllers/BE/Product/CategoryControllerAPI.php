<?php

namespace App\Http\Controllers\BE\Product;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\ProductCategory;

class CategoryControllerAPI extends Controller
{

    public function json(Request $request){
        $level = $request->query('level');
        $parent = $request->query('parent');
        $id = $request->query('id');
        if($parent)  $data = ProductCategory::where('procat_parent', $parent)->orderBy('procat_level', 'asc')->get();
        else if($id)  $data = ProductCategory::where('procat_id', $id)->first();
        else  $data = ProductCategory::orderBy('procat_level', 'asc')->get();
        return response()->json($data);
    }

    public function list()
    {
        $data = ProductCategory::orderBy('procat_level', 'asc')->get();
        return $data;
    }

    public function dt()
    {
        $data = ProductCategory::all();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->procat_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->procat_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = ProductCategory::find($request->procat_id) ?? new ProductCategory();
            if($inp['procat_parent']){
                $parent = ProductCategory::where('procat_id', $inp['procat_parent'])->first();
                $dbs->procat_level = $parent->procat_level + 1;
            }

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return ProductCategory::find($id)->toJson();
    }

    public function reorder(Request $request){
        $data = $request->params;
        foreach($data as $key => $level1){
            $dbs = ProductCategory::find($level1['id']);
            $dbs->procat_parent = null;
            $dbs->procat_level = 1;
            $dbs->save();

            if(isset($level1['children']))
            foreach($level1['children'] as $level2){
                $dbs = ProductCategory::find($level2['id']);
                $dbs->procat_parent = $level1['id'];
                $dbs->procat_level = 2;
                $dbs->save();

                if(isset($level2['children']))
                foreach($level2['children'] as $level3){
                    $dbs = ProductCategory::find($level3['id']);
                    $dbs->procat_parent = $level2['id'];
                    $dbs->procat_level = 3;
                    $dbs->save();

                    if(isset($level3['children']))
                    foreach($level3['children'] as $level4){
                        $dbs = ProductCategory::find($level4['id']);
                        $dbs->procat_parent = $level3['id'];
                        $dbs->procat_level = 4;
                        $dbs->save();
                    }
                }
            }
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menyimpan data',
        ]);
    }

    public function delete($id)
    {
        try {
            ProductCategory::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }
}
