<?php

namespace App\Http\Controllers\BE\Product;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Layout\Layout6;
use App\Models\Layout\Layout6Child;

class Layout6ControllerAPI extends Controller
{

    public function json(Request $request){
        $id = $request->query('id');
        $langcode = $request->query('langcode');
        $lang = \DB::table('languages')->where('lang_code', $langcode)->first();

        $layout = Layout6::where('pl6_lang_id', $lang->lang_id)->where('pl6_procat_id', $id)->orderBy('pl6_order')->get();

        foreach($layout as $item){
            $childs = Layout6Child::where('pl6c_pl6_id', $item->pl6_id)->get();
            $item['childrens'] = $childs;
        }

        return $layout;
    }

    public function dt($id)
    {
        $data = Layout6::join('languages', 'languages.lang_id', '=', 'product_layout6.pl6_lang_id')
            ->join('product_category', 'product_category.procat_id', '=', 'product_layout6.pl6_procat_id')
            ->where('procat_id', $id)->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->pl6_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->pl6_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'pl6_header'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Layout6::find($request->pl6_id) ?? new Layout6();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return Layout6::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Layout6::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    // child
    public function child_dt($id)
    {
        $data = Layout6Child::join('product_layout6', 'product_layout6.pl6_id', '=', 'product_layout6_child.pl6c_pl6_id')
            ->join('languages', 'languages.lang_id', '=', 'product_layout6.pl6_lang_id')
            ->join('product_category', 'product_category.procat_id', '=', 'product_layout6.pl6_procat_id')
            ->where('pl6c_pl6_id', $id)->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->pl6c_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->pl6c_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'pl6c_content'])->toJson();
    }

    public function child_save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Layout6Child::find($request->pl6c_id) ?? new Layout6Child();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function child_getById($id)
    {
        return Layout6Child::find($id)->toJson();
    }

    public function child_delete($id)
    {
        try {
            Layout6Child::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }
}
