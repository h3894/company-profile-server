<?php

namespace App\Http\Controllers\BE\Settings;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\PageContent;
use DB;

class PageContentControllerAPI extends Controller
{
    public function json(Request $request){
        $lang_code = $request->query('lang_code');
        $code = $request->query('code');
        $page = DB::table('pages')->where('page_code', $code)->first();
        $lang = DB::table('languages')->where('lang_code', $lang_code)->first();
        $page_content = [];
        if($page){
            $page_content = PageContent::where('pc_pageid', $page->page_id)->where('pc_langid', $lang->lang_id)->get();
        }
        $temp_content = [];
        foreach($page_content as $key => $item){
            $temp_content[$key] = array(
                'code' => $item->pc_key,
                'content' => $item->pc_content
            );
        }
        return $temp_content;
    }

    public function dt(Request $request)
    {
        $page = $request->page;
        $lang = $request->lang;

        $data = DB::table('page_content')
            ->join('languages', 'page_content.pc_langid', '=', 'languages.lang_id')
            ->join('pages', 'page_content.pc_pageid', '=', 'pages.page_id')
            ->where('pc_pageid', $page)
            ->where('pc_langid', $lang)
            ->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '
                    <a href="javascript:edit(\''.$db->pc_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                    <a href="javascript:del(\''.$db->pc_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>
                ';
            })
            ->rawColumns(['action', 'event_period', 'event_title', 'event_price'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = PageContent::find($request->pc_id) ?? new PageContent();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return PageContent::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            PageContent::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //-----------------------------------------------------------------------

}
