<?php

namespace App\Http\Controllers\BE\Settings;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Teams;

class TeamControllerAPI extends Controller
{

    public function json(Request $request){
        $tem = $request->query('team');
        if($request->query('id')){
            $team = Teams::leftJoin('team_category', 'team_category.tc_id', '=', 'teams.team_team')->where('team_id', $request->query('id'))->first();
            return response()->json($team);
        } else
        return Teams::leftJoin('team_category', 'team_category.tc_id', '=', 'teams.team_team')->where('team_team', $tem)->get();
    }

    public function dt()
    {
        $data = Teams::leftJoin('team_category', 'team_category.tc_id', '=', 'teams.team_team')
            ->get();

        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                return '
                    <a href="javascript:edit(\''.$db->team_id.'\', true)" title="Duplicate Data" class="btn btn-sm btn-icon btn-info"><i class="bx bx-copy"></i></a>
                    <a href="javascript:edit(\''.$db->team_id.'\', false)" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                    <a href="javascript:del(\''.$db->team_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>
                ';
            })
            ->editColumn('team_profile_pict', function($db){
                return '
                    <img src="'.$db->team_profile_pict.'" style="height:50px" />
                ';
            })
            ->editColumn('team_banner_pict', function($db){
                return '
                    <a target="_blank" href="'.$db->team_banner_pict.'">Gambar Banner</a>
                ';
            })
            ->editColumn('team_role', function($db){
                return '
                <div class="d-flex flex-column">
                    <span>'.$db->team_role.'</span>
                    <strong>'.$db->tc_name.'</strong>
                </div>
                ';
            })
            ->rawColumns(['team_banner_pict', 'team_profile_pict', 'team_role', 'team_desc', 'action'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Teams::find($request->team_id) ?? new Teams();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return Teams::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Teams::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //-----------------------------------------------------------------------

}
