<?php

namespace App\Http\Controllers\BE\Settings;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\SocialMedia;

class SocialMediaControllerAPI extends Controller
{

    public function json(Request $request){
        $type = $request->query('type');
        $data = SocialMedia::where('socmed_type', $type)->get();
        return $data;
    }

    public function dt()
    {
        $data = SocialMedia::all();

        return datatables($data)
            ->addIndexColumn()
            ->editColumn('socmed_logo', function($db){
                return '
                    <img src="'.$db->socmed_logo.'" style="height:50px" />
                ';
            })
            ->addColumn('action', function ($db) {
                return '<a href="javascript:edit(\''.$db->socmed_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="bx bx-edit"></i></a>
                <a href="javascript:del(\''.$db->socmed_id.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="bx bx-trash"></i></a>';
            })
            ->rawColumns(['action', 'socmed_logo'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = SocialMedia::find($request->socmed_id) ?? new SocialMedia();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menyimpan data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function getById($id)
    {
        return SocialMedia::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            SocialMedia::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //-----------------------------------------------------------------------

}
