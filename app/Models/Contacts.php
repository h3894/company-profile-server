<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = "mail_contacts";
    protected $primaryKey = 'ct_id';
    public $timestamps = false;
}
