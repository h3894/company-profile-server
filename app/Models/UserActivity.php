<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    protected $table = "articles_user_activity";
    protected $primaryKey = 'activity_id';
    public $timestamps = false;
    protected $fillable = ['activity_type', 'activity_value', 'activity_targetid', 'activity_targettype', 'activity_source', 'activity_userid'];
}
