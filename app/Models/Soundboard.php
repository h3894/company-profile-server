<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Soundboard extends Model
{
    protected $table = "soundboards";
    protected $primaryKey = 'soundboard_id';
    public $timestamps = false;
}
