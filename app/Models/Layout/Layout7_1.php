<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout7_1 extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout7_1';
    protected $primaryKey = 'pl7s1_id';
}
