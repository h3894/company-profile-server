<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout6 extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout6';
    protected $primaryKey = 'pl6_id';
}
