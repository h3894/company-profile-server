<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout4 extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout4';
    protected $primaryKey = 'pl4_id';
}
