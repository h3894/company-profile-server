<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout3 extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout3';
    protected $primaryKey = 'pl3_id';
}
