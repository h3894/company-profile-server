<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout2 extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout2';
    protected $primaryKey = 'pl2_id';
}
