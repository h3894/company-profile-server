<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout1Child extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout1_child';
    protected $primaryKey = 'pl1c_id';
}
