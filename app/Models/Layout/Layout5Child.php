<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout5Child extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout5_child';
    protected $primaryKey = 'pl5c_id';
}
