<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout7_2 extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout7_2';
    protected $primaryKey = 'pl7s2_id';
}
