<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout5 extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout5';
    protected $primaryKey = 'pl5_id';
}
