<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout6Child extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout6_child';
    protected $primaryKey = 'pl6c_id';
}
