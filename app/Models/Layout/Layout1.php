<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout1 extends Model
{
    use SoftDeletes;

    protected $table = 'product_layout1';
    protected $primaryKey = 'pl1_id';
}
