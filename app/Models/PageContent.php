<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    protected $table = "page_content";
    protected $primaryKey = 'pc_id';
    public $timestamps = false;
}
