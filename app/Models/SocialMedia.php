<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $table = "social_medias";
    protected $primaryKey = 'socmed_id';
    public $timestamps = false;
}
