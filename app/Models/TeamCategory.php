<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamCategory extends Model
{
    protected $table = "team_category";
    protected $primaryKey = 'tc_id';
    public $timestamps = false;
}
